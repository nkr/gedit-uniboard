import gi

gi.require_version("Gtk", "3.0")
gi.require_version("Gedit", "3.0")
from gi.repository import GObject, Gtk, Gdk, Gedit
from .charmaps import ipa, greek, special


class uniboardPlugin(GObject.Object, Gedit.ViewActivatable):
    __gtype_name__ = "uniboard"

    view = GObject.property(type=Gedit.View)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        self._handler = None
        self._doc = self.view.get_buffer()
        self._update_active()

    def do_deactivate(self):
        if self._handler:
            self.view.disconnect(self._handler)

    def do_update_state(self):
        pass

    def _update_active(self):
        active = self.view.get_editable() is not None
        if active and self._handler is None:
            self._handler = self.view.connect(
                "key-press-event", self._on_key_press_event
            )
        elif not active and self._handler is not None:
            self.view.disconnect(self._handler)
            self._handler = None

    def _on_key_press_event(self, view, event):
        if event.state & Gdk.ModifierType.MOD1_MASK:
            if event.keyval == Gdk.KEY_i:
                _map = ipa
            elif event.keyval == Gdk.KEY_g:
                _map = greek
            elif event.keyval == Gdk.KEY_u:
                _map = special
            else:
                return False

            insert = self._doc.get_iter_at_mark(self._doc.get_insert())
            insert.backward_char()
            c = insert.get_char()
            if "\u0300" <= c <= "\u036f":
                insert.backward_char()  # extra backward to skip diacritics
                c = insert.get_char()
            u = c.translate(_map).encode("utf-8")
            print(c, c.translate(_map), u, len(u), u.decode("utf-8"))
            insert.forward_char()
            if c == c.translate(_map):
                return False
            print(c, c.translate(_map), u, len(u), u.decode("utf-8"))
            self._doc.backspace(insert, True, True)
            self._doc.insert(insert, u.decode("utf-8"), len(u))
            return True
        return False
