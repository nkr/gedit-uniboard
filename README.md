# Gedit uniboard plugin

Type a character, and press
- `Alt+i` to cycle through IPA characters
- `Alt+g` to cycle through Greek characters
- `Alt+u` to cycle through unicode symbols
